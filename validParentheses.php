<?php
function minRemoveToMakeValid($strInput) 
{
    if($strInput == ''){
        return 'Invalid input';
    }

    $opensParentheses = [];
    $input = str_split($strInput);
    
    for($i = 0; $i < count($input); $i++)
    {
        if($input[$i] == "("){
            $opensParentheses[] = $i;
        }elseif($input[$i] == ")"){
            if(!$opensParentheses){
                $input[$i] = "";
            }else{
                array_pop($opensParentheses);
            }
        }
    }
    while($opensParentheses)
        $input[array_pop($opensParentheses)] = "";

    return implode('',$input);
}


$input = 'lee(t(c)o)de)';
$input = '))((';
$output =  minRemoveToMakeValid($input);

echo "Input : ".$input;
echo "</br>Output : ".$output;
?>