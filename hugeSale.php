<?php
    function getProfit($max_items,$no_of_sale_item,$item_price)
    {
        if($max_items == '' || $no_of_sale_item == '' || $item_price == ''){
            return 'Invalid format';
        }

        if($max_items >= $no_of_sale_item){
            return 'The max item should be less than no of seal item.';
        }

        preg_match_all('!-\d+!', $item_price, $matches);
        $matched = $matches[0]; 
        asort($matched);
        $finalValue = array_slice($matched,0,$max_items);
        $finalValue = (count($finalValue) > 0) ? array_sum($finalValue): '0';
        $finalValue = str_replace('-','',$finalValue);
        return $finalValue;
    }


    $max_items = 2;
    $no_of_sale_item = 3;
    $item_price = "-0 -10 -50";
    $item_price = "0 10 50";
    $output = getProfit($max_items,$no_of_sale_item,$item_price);
    
    echo "Input max_items: ".$max_items;
    echo "</br>Input no_of_sale_item: ".$no_of_sale_item;
    echo "</br>Input item_price: ".$item_price;
    echo "</br></br>Output : ".$output;
?>