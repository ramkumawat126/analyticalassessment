# Logical/Analytical Assessment #

## Instruction
1. Git clone to this repositery or download zip of code.
2. Put this folder in local server like xampp or upload on any live server.
3. Run url with /analyticalassessment.
4. Run the validParentheses.php file to check the "Minimum Remove to Make Valid Parentheses" with input parameter.
5. Same for Huge Sale, to run the hugeSale.php file for "Print the maximum amount of money that John can earn" with input parameter.
---


### 1. Minimum Remove to Make Valid Parentheses ###
Given a string s of '(' , ')' and lowercase English characters.
Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that the resulting parentheses string is valid and return any valid string.
Formally, a parentheses string is valid if and only if:

* It is the empty string, contains only lowercase characters, or
* It can be written as AB (A concatenated with B), where A and B are valid strings, or
* It can be written as (A), where A is a valid string.

#### Answer ####
```PHP 
function minRemoveToMakeValid($strInput) 
{
    if($strInput == ''){
        return 'Invalid input';
    }

    $opensParentheses = [];
    $input = str_split($strInput);
    
    for($i = 0; $i < count($input); $i++)
    {
        if($input[$i] == "("){
            $opensParentheses[] = $i;
        }elseif($input[$i] == ")"){
            if(!$opensParentheses){
                $input[$i] = "";
            }else{
                array_pop($opensParentheses);
            }
        }
    }
    while($opensParentheses)
        $input[array_pop($opensParentheses)] = "";

    return implode('',$input);
}

$case1 = 'lee(t(c)o)de)';
$output =  minRemoveToMakeValid($case1);

echo "Input : ".$case1;
echo "</br>Output : ".$output;
```

#### Example ####
* __Example #1__ Call the function __ echo __minRemoveToMakeValid("lee(t(c)o)de)")__ .
* __Example #2__ Call the function __ echo __minRemoveToMakeValid("a)b(c)d")__ .
* __Example #3__ Call the function __ echo __minRemoveToMakeValid("))((")__ .
---



### 2. Huge Sale ###
In between his coding assignments, John is visiting a neighborhood garage sale to purchase some items for his home.
However, some items are junk and have a negative price — their owners are happy to pay someone to take them away. John has a big truck and can take up to M items. He can only make one trip and needs your help to maximize the money he can make.

#### Input ####
The first line of input contains an integer M, representing the maximum number of items that John can carry in his truck.
The second line of input contains an integer N, representing the number of items for sale.
The third line of input contains N space-separated integers, representing the array elements as prices of the available items.


#### Answer ####
* $max_items variable value represent the  M
* $no_of_sale_item variable value represent the N
* $item_price variable value represent the prices of the available items

```PHP 
    function getMaxProfit($max_items,$no_of_sale_item,$item_price)
    {
        if($max_items == '' || $no_of_sale_item == '' || $item_price == ''){
            return 'Invalid format';
        }

        if($max_items >= $no_of_sale_item){
            return 'The max item should be less than no of seal item.';
        }

        preg_match_all('!-\d+!', $item_price, $matches);
        $matched = $matches[0]; 
        asort($matched);
        $finalValue = array_slice($matched,0,$max_items);
        $finalValue = (count($finalValue) > 0) ? array_sum($finalValue): '0';
        $finalValue = str_replace('-','',$finalValue);
        return $finalValue;
    }


    $max_items = 1;
    $no_of_sale_item = 2;
    $item_price = "-1 -10";
    $output = getMaxProfit($max_items,$no_of_sale_item,$item_price);
    
    echo "Input max_items: ".$max_items;
    echo "</br>Input no_of_sale_item: ".$no_of_sale_item;
    echo "</br>Input item_price: ".$item_price;
    echo "</br></br>Output : ".$output;
```
#### Example ####
* __Example #1__ Call the function __echo getMaxProfit(1,2,"-1 -10")__ .
* __Example #2__ Call the function __echo getMaxProfit(2,3,"10 20 30")__ .

